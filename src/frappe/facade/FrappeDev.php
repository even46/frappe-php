<?php

namespace frappe\facade;

use think\Facade;

/**
 * Class FrappeDev
 * @package frappe\facade
 * @mixin \frappe\FrappeDev
 */
class FrappeDev extends Facade
{
    protected static function getFacadeClass()
    {
        return \frappe\FrappeDev::class;
    }
}
