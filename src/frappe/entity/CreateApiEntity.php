<?php

namespace frappe\entity;

use frappe\utils\ConvertUtil;
use think\facade\Validate;

class CreateApiEntity
{
    /**
     * 数据表名称
     * @var string
     */
    public $tableName;
    /**
     * 默认参数
     * @var array
     */
    public $defaultData = [];
    /**
     * 固定参数
     * @var array
     */
    public $fixedData = [];
    /**
     * 查询字段
     * @var array
     */
    public $tableFields = [];
    /**
     * After Event
     * @var array
     */
    public $afterEvents = [];
    /**
     * @var array
     */
    private $rules = [
        'tableName' => 'require',
        'defaultData' => 'array',
        'fixedData' => 'array',
        'tableFields' => 'array',
        'afterEvents' => 'array',
    ];
    /**
     * @var array
     */
    private $messages = [
        'tableName' => '配置错误',
    ];

    /**
     * 构造数据
     * @param array $config 配置参数
     */
    public function __construct(array $config)
    {
        Validate::rule($this->rules)->message($this->messages)->failException()->check($config);
        $this->tableName = $config['tableName'] ?? "";
        $this->defaultData = $config['defaultData'] ?? [];
        $this->tableFields = $config['tableFields'] ?? [];
        $this->parseConditions($this->tableFields);
        $this->afterEvents = $config['afterEvents'] ?? [];
    }

    protected function parseConditions(array $conditions = [])
    {
        $this->fixedData = [];
        foreach ($conditions as $condition) {
            $name = $condition['name'] ?? "";
            $fixed = $condition['fixed'] ?? null;
            if ($name && !is_null($fixed) && (is_array($fixed) || strlen($fixed) > 0)) {
                $this->fixedData[$name] = $fixed;
            }
        }
    }


    public function toArray(): array
    {
        return [
            'tableName' => $this->tableName,
            'defaultData' => $this->defaultData,
            'fixedData' => $this->fixedData,
            'tableFields' => $this->tableFields,
            'afterEvents' => $this->afterEvents,
        ];
    }
}