<?php

namespace frappe\entity;

use frappe\utils\ConvertUtil;
use think\facade\Validate;

class SelectApiEntity
{
    /**
     * 数据表名称
     * @var mixed string
     */
    public $tableName;
    /**
     * 查询参数
     * @var array
     */
    public $queryParams = [];
    /**
     * 默认参数
     * @var array
     */
    public $defaultParams = [];
    /**
     * 固定参数
     * @var array
     */
    public $fixedParams = [];
    /**
     * 必须参数
     */
    public $requiredParams = [];
    /**
     * 查询条件
     * @var array
     */
    public $conditions = [];
    /**
     * 查询字段
     * @var array
     */
    public $tableFields = [];
    /**
     * 关联数据表配置
     * @var array
     */
    public $tableJoins = [];
    /**
     * 查询排序
     * @var array
     */
    public $queryOrder = [];
    /**
     * 默认排序
     * @var array
     */
    public $defaultOrder = [];
    /**
     * 查询分组
     * @var string
     */
    public $tableGroup = "";
    /**
     * 分页查询
     * @var bool
     */
    public $isPaginate = false;
    /**
     * 树结构
     * @var bool
     */
    public $isTree = false;
    /**
     * @var int
     */
    public $limit = 0;

    /**
     * 配置验证规则
     * @var array
     */
    private $rules = [
        'tableName' => 'require',
    ];
    /**
     * 配置验证错误提示
     * @var array
     */
    private $messages = [
        'tableName' => '配置错误',
    ];

    public function __construct(array $config)
    {
        Validate::rule($this->rules)->message($this->messages)->failException()->check($config);
        $this->tableName = $config['tableName'];
        $this->defaultParams = $config['defaultParams'] ?? [];
        $this->conditions = $config['conditions'] ?? [];
        $this->parseConditions($this->conditions);
        $this->tableFields = $config['tableFields'] ?? [];
        $this->tableJoins = $config['tableJoins'] ?? [];
        $this->defaultOrder = $config['defaultOrder'] ?? [];
        $this->tableGroup = $config['tableGroup'] ?? "";
        $this->isPaginate = $config['isPaginate'] ?? true;
        $this->isTree = $config['isTree'] ?? false;
    }

    protected function parseConditions(array $conditions = [])
    {
        $this->fixedParams = [];
        $this->requiredParams = [];
        foreach ($conditions as $condition) {
            $name = $condition['name'] ?? "";
            $fixed = $condition['fixed'] ?? null;
            $convert = $condition['convert'] ?? null;
            if ($convert && !is_null($fixed) && strlen($fixed) > 0) {
                $fixed = ConvertUtil::convert($convert, $fixed);
            }
            if ($name && !is_null($fixed) && (is_array($fixed) || strlen($fixed) > 0)) {
                $this->fixedParams[$name] = $fixed;
            }
            $required = ($condition['required'] ?? false) ? true : false;
            if ($name && $required) {
                $this->requiredParams[] = $name;
            }
        }
    }

    public function toArray(): array
    {
        return [
            'tableName' => $this->tableName,
            'defaultParams' => $this->defaultParams,
            'fixedParams' => $this->fixedParams,
            'conditions' => $this->conditions,
            'tableFields' => $this->tableFields,
            'tableJoins' => $this->tableJoins,
            'defaultOrder' => $this->defaultOrder,
            'tableGroup' => $this->tableGroup,
            'isPaginate' => $this->isPaginate,
            'isTree' => $this->isTree,
        ];
    }
}