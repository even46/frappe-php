<?php

namespace frappe\entity;

use think\facade\Validate;

class DeleteApiEntity
{
    /**
     * 数据表名称
     * @var string
     */
    public $tableName;
    /**
     * 默认参数
     * @var array
     */
    public $defaultParams = [];
    /**
     * 固定参数
     * @var array
     */
    public $fixedParams = [];
    /**
     * 查询条件
     * @var array
     */
    public $conditions = [];
    /**
     * 关联数据表配置
     * @var array
     */
    public $tableJoins = [];

    /**
     * 配置验证规则
     * @var array
     */
    private $rules = [
        'tableName' => 'require',
    ];
    /**
     * 配置验证错误提示
     * @var array
     */
    private $messages = [
        'tableName' => '配置错误',
    ];

    public function __construct(array $config)
    {
        Validate::rule($this->rules)->message($this->messages)->failException()->check($config);
        $this->tableName = $config['tableName'];
        $this->defaultParams = $config['defaultParams'] ?? [];
        $this->fixedParams = $config['fixedParams'] ?? [];
        $this->conditions = $config['conditions'] ?? [];
        $this->tableJoins = $config['tableJoins'] ?? [];
    }

    public function toArray(): array
    {
        return [
            'tableName' => $this->tableName,
            'defaultParams' => $this->defaultParams,
            'fixedParams' => $this->fixedParams,
            'conditions' => $this->conditions,
            'tableJoins' => $this->tableJoins,
        ];
    }
}