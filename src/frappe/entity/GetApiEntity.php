<?php

namespace frappe\entity;

use think\facade\Log;
use think\facade\Validate;

class GetApiEntity
{
    /**
     * 数据表名称
     * @var mixed string
     */
    public $tableName;
    /**
     * 查询参数
     * @var array
     */
    public $queryParams = [];
    /**
     * 默认参数
     * @var array
     */
    public $defaultParams = [];
    /**
     * 固定参数
     * @var array
     */
    public $fixedParams = [];
    /**
     * 查询条件
     * @var array
     */
    public $conditions = [];
    /**
     * 查询字段
     * @var array
     */
    public $tableFields = [];
    /**
     * 关联数据表配置
     * @var array
     */
    public $tableJoins = [];
    /**
     * 查询排序
     * @var array
     */
    public $queryOrder = [];
    /**
     * 默认排序
     * @var array
     */
    public $defaultOrder = [];
    /**
     * 查询分组
     * @var string
     */
    public $tableGroup = "";

    /**
     * 配置验证规则
     * @var array
     */
    private $rules = [
        'tableName' => 'require',
    ];
    /**
     * 配置验证错误提示
     * @var array
     */
    private $messages = [
        'tableName' => '配置错误',
    ];

    public function __construct(array $config)
    {
        Validate::rule($this->rules)->message($this->messages)->failException()->check($config);
        $this->tableName = $config['tableName'];
        $this->conditions = $config['conditions'] ?? [];
        $this->defaultParams = $config['defaultParams'] ?? [];
        $this->fixedParams = $this->parseConditions($this->conditions);
        $this->tableFields = $config['tableFields'] ?? [];
        $this->tableJoins = $config['tableJoins'] ?? [];
        $this->defaultOrder = $config['defaultOrder'] ?? [];
        $this->tableGroup = $config['tableGroup'] ?? "";
    }

    protected function parseConditions(array $conditions = []): array
    {
        $fixedParams = [];
        foreach ($conditions as $condition) {
            $name = $condition['name'] ?? "";
            $fixed = $condition['fixed'] ?? null;
            if ($name && !is_null($fixed) && strlen($fixed) > 0) {
                $fixedParams[$name] = $fixed;
            }
        }
        return $fixedParams;
    }

    public function toArray(): array
    {
        return [
            'tableName' => $this->tableName,
            'defaultParams' => $this->defaultParams,
            'fixedParams' => $this->fixedParams,
            'conditions' => $this->conditions,
            'tableFields' => $this->tableFields,
            'tableJoins' => $this->tableJoins,
            'defaultOrder' => $this->defaultOrder,
            'tableGroup' => $this->tableGroup,
        ];
    }
}