<?php

namespace frappe\entity;

use think\facade\Validate;

/**
 * 页面
 */
class PageEntity
{
    /**
     * ID
     * @var string
     */
    public $id = "";
    /**
     * @var string 上级ID
     */
    public $pid = "";
    /**
     * 客户端：admin\web\h5\app
     * @var array
     */
    public $client = [];
    /**
     * 标识
     * @var string
     */
    public $name = "";
    /**
     * 类型：GROUP, PAGE, CUSTOM（分组、页面、自定义）
     * @var string
     */
    public $type;
    /**
     * 标题
     * @var string
     */
    public $title;
    public $status;
    /**
     * 注释
     * @var mixed|string
     */
    public $comment;
    /**
     * 配置
     * @var array
     */
    public $config;
    /**
     * @var array 内组件
     */
    public $children;
    /**
     * @var array
     */
    private $rules = [
        'id' => 'require',
        'type' => 'require|in:GROUP,PAGE,TABLE,FORM,CONFIG'
    ];
    /**
     * @var array
     */
    private $messages = [
        'id' => '标识错误',
        'type' => 'Type类型错误'
    ];

    public function __construct(array $config)
    {
        $config['type'] = strtoupper($config['type'] ?? "");
        Validate::rule($this->rules)->message($this->messages)->failException()->check($config);
        $this->id = $config['id'] ?? "";
        $this->pid = $config['pid'] ?? "0";
        $this->client = $config['client'] ?? [];
        $this->name = $config['name'] ?? "";
        $this->type = $config['type'] ?? "";
        $this->title = $config['title'] ?? "";
        $this->comment = $config['comment'] ?? "";
        $this->config = $config['config'] ?? [];
        $this->children = $config['children'] ?? [];
        $this->status = $config['status'] ?? "NO";
    }

    public function toArray(): array
    {
        return [
            "id" => $this->id,
            "pid" => $this->pid,
            "client" => $this->client,
            "name" => $this->name,
            "type" => $this->type,
            "title" => $this->title,
            "comment" => $this->comment,
            "config" => $this->config,
            "children" => $this->children,
            "status" => $this->status,
        ];
    }
    
    public function toInsertArray(): array
    {
        return [
            "id" => $this->id,
            "pid" => $this->pid,
            "client" => json_encode($this->client ?? [], 320),
            "name" => $this->name,
            "type" => $this->type,
            "title" => $this->title,
            "comment" => $this->comment,
            "config" => json_encode($this->config ?? [], 320),
            "children" => json_encode($this->children ?? [], 320),
            "status" => $this->status,
        ];
    }
}