<?php

namespace frappe\entity;

class ConditionEntity
{
    protected $name;
    protected $option;
    protected $required;
    protected $type;
    protected $fixed;
    protected $raw;
}