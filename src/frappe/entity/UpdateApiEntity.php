<?php

namespace frappe\entity;

use think\facade\Validate;
use frappe\utils\ConvertUtil;

class UpdateApiEntity
{
    /**
     * 数据表名称
     * @var string
     */
    public $tableName;
    /**
     * 查询参数
     * @var array
     */
    public $queryParams = [];
    /**
     * 固定参数
     * @var array
     */
    public $fixedParams = [];
    /**
     * 默认参数
     * @var array
     */
    public $defaultParams = [];
    /**
     * 查询条件
     * @var array
     */
    public $conditions = [];
    /**
     * 默认参数
     * @var array
     */
    public $defaultData = [];
    /**
     * 固定参数
     * @var array
     */
    public $fixedData = [];
    /**
     * 查询字段
     * @var array
     */
    public $tableFields = [];
    /**
     * After Event
     * @var array
     */
    public $afterEvents = [];
    /**
     * @var array
     */
    private $rules = [
        'tableName' => 'require',
        'defaultData' => 'array',
        'fixedData' => 'array',
        'tableFields' => 'array',
        'afterEvents' => 'array',
    ];
    /**
     * @var array
     */
    private $messages = [
        'tableName' => '配置错误',
    ];

    /**
     * 构造数据
     * @param array $config 配置参数
     */
    public function __construct(array $config)
    {
        Validate::rule($this->rules)->message($this->messages)->failException()->check($config);
        $this->tableName = $config['tableName'] ?? "";
        $this->conditions = $config['conditions'] ?? [];
        $this->tableFields = $config['tableFields'] ?? [];
        $this->parseConditions($this->tableFields);
//        $this->defaultParams = $config['defaultParams'] ?? [];
//        $this->fixedParams = $config['fixedParams'] ?? [];
//        $this->defaultData = $config['defaultData'] ?? [];
//        $this->fixedData = $config['fixedData'] ?? [];
        $this->afterEvents = $config['afterEvents'] ?? [];
    }

    # 解析查询参数
    public function setQueryParams(array $params)
    {
        $this->queryParams = [];
        $this->conditions = ConvertUtil::convertByGlobal($this->conditions);
        foreach ($this->conditions as $condition) {
            $name = $condition['name'] ?? '';
            $required = (bool)($condition['required'] ?? false);
            $type = $condition['type'] ?? 'text';
            $fixed = $condition['fixed'] ?? '';
            $type = $condition['type'] ?? 'text';
            $value = '';
            if ($fixed) {
                $value = ConvertUtil::convertFixed($fixed);
                $this->queryParams[$name] = ConvertUtil::convertType($type, $value);
            } else {
                if ($required && !isset($params[$name])) {
                    throw new \Exception('请求参数错误');
                }
                if (isset($params[$name])) {
                    $this->queryParams[$name] = ConvertUtil::convertType($type, $params[$name]);
                }
            }
        }
    }
    
    protected function parseConditions(array $conditions = [])
    {
        $this->fixedData = [];
        foreach ($conditions as $condition) {
            $name = $condition['name'] ?? "";
            $fixed = $condition['fixed'] ?? null;
            if ($name && !is_null($fixed) && (is_array($fixed) || strlen($fixed) > 0)) {
                $this->fixedData[$name] = $fixed;
            }
        }
    }


    public function toArray(): array
    {
        return [
            'tableName' => $this->tableName,
            'defaultParams' => $this->defaultParams,
            'fixedParams' => $this->fixedParams,
            'conditions' => $this->conditions,
            'defaultData' => $this->defaultData,
            'fixedData' => $this->fixedData,
            'tableFields' => $this->tableFields,
            'afterEvents' => $this->afterEvents,
        ];
    }
}