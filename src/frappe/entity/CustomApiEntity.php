<?php

namespace frappe\entity;

use think\facade\Validate;

class CustomApiEntity
{
    /**
     * 代码标识
     * @var string
     */
    public $name;
    /**
     * 代码
     * @var string
     */
    public $content = "";
    /**
     * 配置验证规则
     * @var array
     */
    private $rules = [
        'name' => 'require',
    ];
    /**
     * 配置验证错误提示
     * @var array
     */
    private $messages = [
        'name' => 'Custom Api Config Error By Name',
    ];

    public function __construct(array $config)
    {
        Validate::rule($this->rules)->message($this->messages)->failException()->check($config);
        $this->name = ucfirst($config['name']);
        $this->content = $config['content'] ?? "";
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'content' => $this->content,
        ];
    }
}