<?php

namespace frappe\entity;

use think\facade\Validate;

class ApiEntity
{
    /**
     * ID
     * @var string
     */
    public $id = "";
    /**
     * PID
     * @var string 
     */
    public $pid = "";
    /**
     * 客户端：admin\web\h5\app
     * @var array
     */
    public $client = [];
    /**
     * 标识
     * @var string
     */
    public $name = "";
    /**
     * 类型：SELECT,GET,CREATE,UPDATE,SET,DELETE,IMPORT,EXPORT,OPTION,CUSTOM,GROUP
     * @var string
     */
    public $type;
    /**
     * 标题
     * @var string
     */
    public $title;
    public $signature;
    public $encrypt;
    public $authorization;
    public $permission;
    public $log;
    public $log_has_param;
    public $status;
    /**
     * 注释
     * @var mixed|string
     */
    public $comment;
    /**
     * 配置
     * @var array
     */
    public $config;
    /**
     * @var array
     */
    private $rules = [
        'id' => 'require',
        'type' => 'require|in:SELECT,GET,CREATE,UPDATE,SET,DELETE,OPTION,IMPORT,EXPORT,CUSTOM,GROUP'
    ];
    /**
     * @var array
     */
    private $messages = [
        'id' => '标识错误',
        'type' => '配置错误'
    ];

    public function __construct(array $config)
    {
        $config['type'] = strtoupper($config['type'] ?? "");
        Validate::rule($this->rules)->message($this->messages)->failException()->check($config);
        $this->id = $config['id'] ?? "";
        $this->pid = $config['pid'] ?? "0";
        $this->client = $config['client'] ?? [];
        $this->name = $config['name'] ?? "";
        $this->type = $config['type'] ?? "";
        $this->title = $config['title'] ?? "";
        $this->comment = $config['comment'] ?? "";
        $this->config = $config['config'] ?? [];
        $this->signature = $config['signature'] ?? "NO";
        $this->encrypt = $config['encrypt'] ?? "NO";
        $this->authorization = $config['authorization'] ?? "NO";
        $this->permission = $config['permission'] ?? "NO";
        $this->log = $config['log'] ?? "NO";
        $this->log_has_param = $config['log_has_param'] ?? "NO";
        $this->status = $config['status'] ?? "NO";
    }

    public function toArray(): array
    {
        return [
            "id" => $this->id,
            "pid" => $this->pid,
            "client" => $this->client,
            "name" => $this->name,
            "type" => $this->type,
            "title" => $this->title,
            "comment" => $this->comment,
            "config" => $this->config,
            "signature" => $this->signature,
            "encrypt" => $this->encrypt,
            "authorization" => $this->authorization,
            "permission" => $this->permission,
            "log" => $this->log,
            "log_has_param" => $this->log_has_param,
            "status" => $this->status,
        ];
    }
}