<?php

namespace frappe;

use frappe\entity\ApiEntity;
use frappe\entity\ComponentEntity;
use frappe\entity\CreateApiEntity;
use frappe\entity\CustomApiEntity;
use frappe\entity\DeleteApiEntity;
use frappe\entity\GetApiEntity;
use frappe\entity\OptionApiEntity;
use frappe\entity\PageEntity;
use frappe\entity\SelectApiEntity;
use frappe\entity\UpdateApiEntity;
use frappe\utils\ConfigUtil;
use frappe\utils\CustomUtil;
use think\Collection;
use think\Config;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\Db;
use think\Request;
use InvalidArgumentException;

/**
 * 开发专用
 */
class FrappeDev
{
    protected $request;
    /**
     * @var ApiEntity
     */
    protected $apiEntity;
    /**
     * @var ComponentEntity
     */
    protected $componentEntity;
    /**
     * @var PageEntity
     */
    protected $pageEntity;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    # 执行相关操作
    public function action(string $method)
    {
        if (!in_array($method, [
                'schema', 'client',
                'queryApi', 'findApi', 'createApi', 'updateApi', 'deleteApi',
                'queryPage', 'findPage', 'createPage', 'updatePage', 'deletePage',
                'queryComponent', 'findComponent', 'createComponent', 'updateComponent', 'deleteComponent',
            ]) || !method_exists($this, $method)) {
            throw new \InvalidArgumentException('无效的Action');
        }
        return $this->$method();
    }

    # 客户端
    protected function client()
    {
        return config('frappe.client', []);
    }

    /**
     * 获取数据表
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     * @author yinxu
     * @date 2024/3/24 18:40:57
     */
    protected function schema(): array
    {
        $tables = ConfigUtil::querySchemaTables();
        foreach ($tables as $key => &$table) {
            if (in_array($table['tableName'], [
                ConfigUtil::getTableName('api'),
                ConfigUtil::getTableName('component'),
                ConfigUtil::getTableName('page'),
            ])) {
                unset($tables[$key]);
                continue;
            }
            $table['columns'] = ConfigUtil::querySchemaTableColumns($table['tableName']);
        }
        return array_merge($tables, []);
    }

    # 查询API
    protected function queryApi()
    {
        $data = Db::table(ConfigUtil::getTableName('api'))->field('id,pid,client,name,type,title,status')->order('id desc')->select();
        if ($data instanceof Collection) {
            $data->each(function ($item) {
                $item['type'] = strtoupper($item['type'] ?? "");
                return $item;
            });
        }
        $trees = [];
        arr2tree($data ? $data->toArray() : [],'id','pid','0',$trees);
        return $trees;
    }

    # 查询单条数据
    protected function findApi()
    {
        $id = $this->request->param('id/s', '', 'trim');
        if (!$id) throw new \InvalidArgumentException('无效ID参数');
        $data = Db::table(ConfigUtil::getTableName('api'))->find($id);
        if (!$data) throw new DataNotFoundException('无数据');
        $data['config'] = $data['config'] ? json_decode($data['config'], true) : null;
        $data['type'] = strtoupper($data['type'] ?? "");
        $data['client'] = explode(',', $data['client']) ?? [];
        return $data;
    }

    # 新增API
    protected function createApi(): array
    {
        try {
            $options = $this->request->except(['__action']);
            $this->apiEntity = new ApiEntity($options);

            # 验证ID
            if (Db::table(ConfigUtil::getTableName('api'))->where('id', $this->apiEntity->id)->count() > 0) {
                throw new InvalidArgumentException("id: {$this->apiEntity->id} 参数无效");
            }
            # 验证Name
            if ($this->apiEntity->name && Db::table(ConfigUtil::getTableName('api'))->where('name', $this->apiEntity->name)->count() > 0) {
                throw new InvalidArgumentException("name: {$this->apiEntity->name} 参数无效");
            }

            # 执行数据
            switch ($this->apiEntity->type) {
                case 'SELECT':
                    $this->apiEntity->config = (new SelectApiEntity($this->apiEntity->config))->toArray();
                    break;
                case 'GET':
                    $this->apiEntity->config = (new GetApiEntity($this->apiEntity->config))->toArray();
                    break;
                case 'CREATE':
                    $this->apiEntity->config = (new CreateApiEntity($this->apiEntity->config))->toArray();
                    break;
                case 'UPDATE':
                    $this->apiEntity->config = (new UpdateApiEntity($this->apiEntity->config))->toArray();
                    break;
                case 'DELETE':
                    $this->apiEntity->config = (new DeleteApiEntity($this->apiEntity->config))->toArray();
                    break;
                case 'OPTION':
                    $this->apiEntity->config = (new OptionApiEntity($this->apiEntity->config))->toArray();
                    break;
                case 'CUSTOM':
                    $customApiEntity = new CustomApiEntity($this->apiEntity->config);
                    CustomUtil::write($customApiEntity->name, $customApiEntity->content);
                    $this->apiEntity->config = $customApiEntity->toArray();
                    break;
                case 'GROUP':
                    if ($this->apiEntity->pid != "0") {
                        if (Db::table(ConfigUtil::getTableName('api'))->where('id', $this->apiEntity->pid)->count() < 1) {
                            throw new InvalidArgumentException("pid: {$this->apiEntity->pid} 参数无效");
                        }
                    }
                    break;
                default:
                    throw new InvalidArgumentException("{$this->apiEntity->title} 配置无效");
            }
            $res = ConfigUtil::createApi($this->apiEntity->toArray());
            if (!$res) {
                throw new \Exception('未知错误');
            }

            return $this->apiEntity->toArray();
        } catch (\Throwable $t) {
            if (!empty($this->apiEntity) && $this->apiEntity->type == 'CUSTOM' && !empty($customApiEntity)) {
                CustomUtil::delete($customApiEntity->name);
            }
            throw $t;
        }
    }

    # 编辑API
    protected function updateApi(): array
    {
        try {
            $id = $this->request->param('id/s', '');
            if (!$id) {
                throw new InvalidArgumentException("Parameter ID Cannot Empty!");
            }
            $oldConfig = ConfigUtil::getApiConfig($id, false);
            $oldApiEntity = new ApiEntity($oldConfig);

            $options = $this->request->except(['__action']);
            $this->apiEntity = new ApiEntity($options);

            # 验证Name
            if ($this->apiEntity->name && Db::table(ConfigUtil::getTableName('api'))->where('id', '<>', $this->apiEntity->id)->where('name', $this->apiEntity->name)->count() > 0) {
                throw new InvalidArgumentException("name: {$this->apiEntity->name} 参数无效");
            }

            // 执行数据
            switch ($this->apiEntity->type) {
                case 'SELECT':
                    $this->apiEntity->config = (new SelectApiEntity($this->apiEntity->config))->toArray();
                    break;
                case 'GET':
                    $this->apiEntity->config = (new GetApiEntity($this->apiEntity->config))->toArray();
                    break;
                case 'CREATE':
                    $this->apiEntity->config = (new CreateApiEntity($this->apiEntity->config))->toArray();
                    break;
                case 'UPDATE':
                    $this->apiEntity->config = (new UpdateApiEntity($this->apiEntity->config))->toArray();
                    break;
                case 'DELETE':
                    $this->apiEntity->config = (new DeleteApiEntity($this->apiEntity->config))->toArray();
                    break;
                case 'OPTION':
                    $this->apiEntity->config = (new OptionApiEntity($this->apiEntity->config))->toArray();
                    break;
                case 'CUSTOM':
                    $oldCustomApiEntity = new CustomApiEntity($oldApiEntity->config);
                    $customApiEntity = new CustomApiEntity($this->apiEntity->config);
                    if ($oldCustomApiEntity->name != $customApiEntity->name) {
                        throw new InvalidArgumentException("Prohibit Changes $oldCustomApiEntity->name -> $customApiEntity->name");
                    }
                    CustomUtil::write($customApiEntity->name, $customApiEntity->content);
                    $this->apiEntity->config = $customApiEntity->toArray();
                    break;
                case 'GROUP':
                    if ($this->apiEntity->pid != "0") {
                        if (Db::table(ConfigUtil::getTableName('api'))->where('id', $this->apiEntity->pid)->count() < 1) {
                            throw new InvalidArgumentException("pid: {$this->apiEntity->pid} 参数无效");
                        }
                    }
                    break;
                default:
                    throw new InvalidArgumentException("{$this->apiEntity->title} 配置无效");
            }
            $res = ConfigUtil::updateApi($this->apiEntity->id, $this->apiEntity->toArray());
            if (!$res) {
                throw new \Exception('数据未更新');
            }

            return $this->apiEntity->toArray();
        } catch (\Throwable $t) {
            if (!empty($this->apiEntity) && $this->apiEntity->type == 'CUSTOM' && !empty($oldCustomApiEntity)) {
                CustomUtil::write($oldCustomApiEntity->name, $oldCustomApiEntity->content);
            }
            throw $t;
        }
    }

    # 删除API
    protected function deleteApi(): bool
    {
        $id = $this->request->param('id/s', '');
        if (!$id) {
            throw new InvalidArgumentException("Parameter ID Cannot Empty!");
        }
        $oldConfig = ConfigUtil::getApiConfig($id);
        # 判断是否存在子集
        if (Db::table(ConfigUtil::getTableName('api'))->where('pid', $id)->count() > 0) {
            throw new InvalidArgumentException("当前分组下存在多个子接口，请先删除");
        }
        $oldApiEntity = new ApiEntity($oldConfig);
        if ($oldApiEntity->type == 'CUSTOM') {
            $oldCustomApiEntity = new CustomApiEntity($oldApiEntity->config);
            CustomUtil::delete($oldCustomApiEntity->name);
        }
        $res = ConfigUtil::deleteApi($id);
        if (!$res) throw new DbException('系统繁忙');
        return true;
    }

    # 查询Page
    protected function queryPage()
    {
        $data = Db::table(ConfigUtil::getTableName('page'))->field('id,pid,client,name,type,title,status')->order('id desc')->select();
        if ($data instanceof Collection) {
            $data->each(function ($item) {
                $item['type'] = strtoupper($item['type'] ?? "");
                return $item;
            });
        }
        $trees = [];
        arr2tree($data ? $data->toArray() : [],'id','pid','0',$trees);
        return $trees;
    }

    # 查询单Page
    protected function findPage()
    {
        $id = $this->request->param('id/s', '', 'trim');
        if (!$id) throw new \InvalidArgumentException('无效ID参数');
        $data = Db::table(ConfigUtil::getTableName('page'))->find($id);
        if (!$data) throw new DataNotFoundException('无数据');
        $data['config'] = $data['config'] ? json_decode($data['config'], true) : [];
        $data['children'] = $data['children'] ? json_decode($data['children'], true) : [];
        $data['type'] = strtoupper($data['type'] ?? "");
        return $data;
    }

    # createPage
    protected function createPage(): array
    {
        $options = $this->request->except(['__action']);
        $this->pageEntity = new PageEntity($options);
        # 验证ID
        if (Db::table(ConfigUtil::getTableName('page'))->where('id', $this->pageEntity->id)->count() > 0) {
            throw new InvalidArgumentException("id: {$this->pageEntity->id} 参数无效");
        }
        # 验证Name不重复
        if ($this->pageEntity->name && Db::table(ConfigUtil::getTableName('page'))->where('name', $this->pageEntity->name)->count() > 0) {
            throw new InvalidArgumentException("name: {$this->pageEntity->name} 参数无效");
        }
        # 执行数据
        $res = Db::table(ConfigUtil::getTableName('page'))->insert($this->pageEntity->toInsertArray());
        if (!$res) {
            throw new \Exception('未知错误');
        }
        return $this->pageEntity->toArray();
    }

    # 编辑Component
    protected function updatePage(): array
    {
        $id = $this->request->param('id/s', '');
        if (!$id) {
            throw new InvalidArgumentException("Parameter ID Cannot Empty!");
        }
        $oldConfig = ConfigUtil::getConfig('page', $id, false);
        $options = $this->request->except(['__action']);
        $this->pageEntity = new PageEntity($options);
        $config = $this->pageEntity->toInsertArray();
        unset($config['id']);
        # 验证Name不重复
        if ($this->pageEntity->name && Db::table(ConfigUtil::getTableName('page'))->where('id', '<>', $id)->where('name', $this->pageEntity->name)->count() > 0) {
            throw new InvalidArgumentException("name: {$this->pageEntity->name} 参数无效");
        }
        $res = Db::table(ConfigUtil::getTableName('page'))->where('id', $id)->update($config);
        if (!$res) {
            throw new \Exception('数据未更新');
        }
        return $this->pageEntity->toArray();
    }

    # 删除Component
    protected function deletePage(): bool
    {
        $id = $this->request->param('id/s', '');
        if (!$id) {
            throw new InvalidArgumentException("Parameter ID Cannot Empty!");
        }
        $oldConfig = ConfigUtil::getConfig('page', $id, false);
        # 判断是否存在子集
        if (Db::table(ConfigUtil::getTableName('page'))->where('pid', $id)->count() > 0) {
            throw new InvalidArgumentException("当前分组下存在多个子接口，请先删除");
        }
        $res = Db::table(ConfigUtil::getTableName('page'))->where('id', $id)->delete();
        if (!$res) throw new DbException('系统繁忙');
        return true;
    }


    # 查询Component
    protected function queryComponent()
    {
        $data = Db::table(ConfigUtil::getTableName('component'))->field('id,pid,client,name,type,title,status')->order('id desc')->select();
        if ($data instanceof Collection) {
            $data->each(function ($item) {
                $item['type'] = strtoupper($item['type'] ?? "");
                return $item;
            });
        }
        $trees = [];
        arr2tree($data ? $data->toArray() : [],'id','pid','0',$trees);
        return $trees;
    }

    # 查询当条数据
    protected function findComponent()
    {
        $id = $this->request->param('id/s', '', 'trim');
        if (!$id) throw new \InvalidArgumentException('无效ID参数');
        $data = Db::table(ConfigUtil::getTableName('component'))->find($id);
        if (!$data) throw new DataNotFoundException('无数据');
        $data['config'] = $data['config'] ? json_decode($data['config'], true) : [];
        $data['component'] = $data['component'] ? json_decode($data['component'], true) : [];
        $data['children'] = $data['children'] ? json_decode($data['children'], true) : [];
        $data['type'] = strtoupper($data['type'] ?? "");
        $data['client'] = explode(',', $data['client']) ?? [];
        return $data;
    }

    # 新增Component
    protected function createComponent(): array
    {
        try {
            $options = $this->request->except(['__action']);
            $this->componentEntity = new ComponentEntity($options);
            # 验证ID
            if (Db::table(ConfigUtil::getTableName('component'))->where('id', $this->componentEntity->id)->count() > 0) {
                throw new InvalidArgumentException("id: {$this->componentEntity->id} 参数无效");
            }
            # 执行数据
            $res = Db::table(ConfigUtil::getTableName('component'))->insert($this->componentEntity->toInsertArray());
            if (!$res) {
                throw new \Exception('未知错误');
            }

            return $this->componentEntity->toArray();
        } catch (\Throwable $t) {
            throw $t;
        }
    }

    # 编辑Component
    protected function updateComponent(): array
    {
        try {
            $id = $this->request->param('id/s', '');
            if (!$id) {
                throw new InvalidArgumentException("Parameter ID Cannot Empty!");
            }
            $oldConfig = ConfigUtil::getConfig('component', $id, false);
            $options = $this->request->except(['__action']);
            $this->componentEntity = new ComponentEntity($options);
            $config = $this->componentEntity->toInsertArray();
            unset($config['id']);
            $res = Db::table(ConfigUtil::getTableName('component'))->where('id', $id)->update($config);
            if (!$res) {
                throw new \Exception('数据未更新');
            }
            return $this->componentEntity->toArray();
        } catch (\Throwable $t) {
            throw $t;
        }
    }

    # 删除Component
    protected function deleteComponent(): bool
    {
        $id = $this->request->param('id/s', '');
        if (!$id) {
            throw new InvalidArgumentException("Parameter ID Cannot Empty!");
        }
        $oldConfig = ConfigUtil::getConfig('component', $id);
        # 判断是否存在子集
        if (Db::table(ConfigUtil::getTableName('component'))->where('pid', $id)->count() > 0) {
            throw new InvalidArgumentException("当前分组下存在多个子接口，请先删除");
        }
        $res = Db::table(ConfigUtil::getTableName('component'))->where('id', $id)->delete();
        if (!$res) throw new DbException('系统繁忙');
        return true;
    }
}