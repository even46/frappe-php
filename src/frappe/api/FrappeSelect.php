<?php
declare (strict_types=1);

namespace frappe\api;

use frappe\utils\ConditionUtil;
use frappe\entity\SelectApiEntity;
use frappe\utils\ConvertUtil;
use think\Collection;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\db\Query;
use think\facade\Db;
use think\Request;

/**
 * 列表查询
 */
class FrappeSelect
{
    /**
     * @var \app\Request|Request
     */
    protected $request;
    /**
     * 数据库查询对象
     * @var Query|Db
     */
    protected $db;
    /**
     * @var SelectApiEntity
     */
    protected $entity;
    /**
     * 数据库查询原始数据
     * @var mixed
     */
    public $originalData;
    /**
     * 响应数据
     * @var mixed
     */
    public $responseData;
    /**
     * 缓存字段数据
     * @var array
     */
    public $_CacheTableFields = [];
    /**
     * 缓存字段需转换的配置
     * @var array
     */
    public $_CacheTableFieldConverts = [];

    /**
     * @param Request $request
     * @param array $config
     */
    public function __construct(Request $request, array $config)
    {
        $this->request = $request;
        $this->entity = new SelectApiEntity($config);
        $this->db = Db::name($this->entity->tableName)->alias($this->entity->tableName);
    }

    /**
     * @param Request $request
     * @param array $config
     * @return FrappeSelect
     * @author yinxu
     * @date 2024/3/23 10:47:56
     */
    public static function load(Request $request, array $config): FrappeSelect
    {
        return new FrappeSelect($request, $config);
    }

    /**
     * 执行查询
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function select()
    {
        $this->entity->limit = $this->request->param('limit/d', 0);
        $this->entity->queryParams = $this->request->param('params/a', []);
        $sorts = $this->request->param('sort/a', []);
        $this->entity->queryOrder = [];
        foreach ($sorts as $key => $sort) {
            $this->entity->queryOrder[] = [
                'name' => $key,
                'sort' => strtolower($sort) == 'descend' ? 'desc':'asc'
            ];
        }
        $this->entity->fixedParams = ConvertUtil::convertByGlobal($this->entity->fixedParams);
        $this->joins()->fields()->where()->order()->group();
        if ($this->entity->isPaginate) {
            $this->originalData = $this->db->paginate($this->entity->limit ?: 10);
        } else {
            if ($this->entity->limit) {
                $this->originalData = $this->db->limit($this->entity->limit)->select();
            } else {
                $this->originalData = $this->db->select();
            }
        }
        $this->convertData();
        return $this->responseData;
    }

    /**
     * Join关联查询表
     * @return $this
     */
    protected function joins(): FrappeSelect
    {
        foreach ($this->entity->tableJoins as $join) {
            $joinName = $join['name'];
            $joinAlias = $join['alias'] ?? $joinName;
            $joinCondition = $join['condition'];
            $joinType = $join['type'] ?? "left";
            $this->db = $this->db->join("$joinName $joinAlias", $joinCondition, $joinType);
        }
        return $this;
    }

    /**
     * 获取查询字段
     * @return $this
     */
    protected function fields(): FrappeSelect
    {
        $this->_CacheTableFields = [];
        foreach ($this->entity->tableFields as $field) {
            $name = $field['name'] ?? "";
            $raw = $field['raw'] ?? "";
            $convert = $field['convert'] ?? "";

            if (empty($name)) continue;
            if ($raw) {
                $field = "$raw as $name";
            } else {
                $field = $this->entity->tableName . "." . $name;
            }

            # 查询字段
            $this->db = $raw ? $this->db->fieldRaw($field) : $this->db->field($field);
            $this->_CacheTableFields[] = $field;
            if ($convert) $this->_CacheTableFieldConverts[$name] = $convert;
        }
        return $this;
    }

    /**
     * 查询数据
     * @return $this
     */
    protected function where(): FrappeSelect
    {
        [$wheres, $rawWheres] = ConditionUtil::load($this->entity->tableName, $this->entity->conditions,
            $this->entity->queryParams, $this->entity->defaultParams,
            $this->entity->fixedParams,
            $this->entity->requiredParams
        )->build();
        if ($wheres) $this->db = $this->db->where($wheres);
        foreach ($rawWheres as $rawWhere) {
            $this->db = $this->db->whereRaw($rawWhere);
        }
        return $this;
    }

    /**
     * 字段排序查询
     * @return $this
     */
    protected function order(): FrappeSelect
    {
        $queryOrders = $this->entity->queryOrder ?: $this->entity->defaultOrder ?? [];
        foreach ($queryOrders as $queryOrder) {
            $this->db = $this->db->order($queryOrder['name'], strtolower(trim($queryOrder['sort'])) == "desc" ? "desc" : "asc");
        }
        return $this;
    }

    /**
     * 分组查询
     * @return $this
     */
    protected function group(): FrappeSelect
    {
        if ($this->entity->tableGroup) {
            $this->db = $this->db->group($this->entity->tableGroup);
        }
        return $this;
    }

    /**
     * 将数据格式化输出
     * @return $this
     */
    protected function convertData(): FrappeSelect
    {
        # 先格式化数据
        $this->responseData = $this->originalData;
        $this->responseData->each(function ($item) {
            foreach ($this->_CacheTableFieldConverts as $name => $convert) {
                if (!isset($item[$name])) continue;
                $item[$name] = ConvertUtil::convert($convert, $item[$name], $item);
            }
            return $item;
        });
        # 在转Tree结构
        if ($this->entity->isTree) {
            $data = $this->responseData;
            if ($data instanceof Collection) {
                $data = $data->toArray();
            }
            if (is_array($data)) {
                $treeList = [];
                arr2tree($data, 'id', 'pid', 0, $treeList, 'children');
                $this->responseData = $treeList;
            }
        }
        return $this;
    }
}