<?php

namespace frappe\api;

use frappe\entity\DeleteApiEntity;
use frappe\utils\ConditionUtil;
use frappe\utils\ConvertUtil;
use think\db\exception\DbException;
use think\db\Query;
use think\facade\Db;
use think\Request;

class FrappeDelete
{
    /**
     * @var \app\Request|Request
     */
    protected $request;
    /**
     * 数据库查询对象
     * @var Query|Db
     */
    protected $db;
    /**
     * @var DeleteApiEntity
     */
    public $entity;
    /**
     * 查询参数
     * @var array
     */
    public $queryParams = [];

    /**
     * 构造数据
     * @param array $config 配置参数
     */
    public function __construct(Request $request, array $config)
    {
        $this->request = $request;
        $this->entity = new DeleteApiEntity($config);
        $this->db = Db::name($this->entity->tableName);
    }

    /**
     * 加载配置
     * @param Request $request
     * @param array $config
     * @return FrappeDelete
     */
    public static function load(Request $request, array $config): FrappeDelete
    {
        return new FrappeDelete($request, $config);
    }

    /**
     * 执行删除
     * @return int
     * @throws DbException
     */
    public function delete(): int
    {
        # 查询数据
        $this->queryParams = $this->request->param() ?? [];
        $this->entity->fixedParams = ConvertUtil::convertByGlobal($this->entity->fixedParams);
        $this->joins()->where();
        return $this->db->delete();
    }

    /**
     * Join关联查询表
     * @return $this
     */
    protected function joins(): FrappeDelete
    {
        foreach ($this->entity->tableJoins as $join) {
            $joinName = $join['name'];
            $joinAlias = $join['alias'] ?? $joinName;
            $joinCondition = $join['condition'];
            $joinType = $join['type'] ?? "left";
            $this->db = $this->db->join("$joinName $joinAlias", $joinCondition, $joinType);
        }
        return $this;
    }

    /**
     * 查询数据
     * @return $this
     */
    protected function where(): FrappeDelete
    {
        [$wheres, $rawWheres] = ConditionUtil::load($this->entity->tableName, $this->entity->conditions, $this->queryParams, $this->entity->defaultParams, $this->entity->fixedParams)->build(false);
        if (!$wheres && !$rawWheres) throw new \Exception('查询条件不存在');
        if ($wheres) $this->db = $this->db->where($wheres);
        foreach ($rawWheres as $rawWhere) {
            $this->db = $this->db->whereRaw($rawWhere);
        }
        return $this;
    }
}