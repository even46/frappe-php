<?php

namespace frappe\api;

use frappe\entity\OptionApiEntity;
use think\Collection;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\Request;

/**
 * Options查询
 */
class FrappeOption
{
    /**
     * @var \app\Request|Request
     */
    protected $request;
    /**
     * @var OptionApiEntity
     */
    protected $entity;

    /**
     * 构造数据
     * @param array $config 配置参数
     */
    public function __construct(Request $request, array $config)
    {
        $this->request = $request;
        $this->entity = new OptionApiEntity($config);
    }

    /**
     * @param Request $request
     * @param array $config
     * @return FrappeOption
     */
    public static function load(Request $request, array $config): FrappeOption
    {
        return new FrappeOption($request, $config);
    }

    /**
     * 获取List<Option>数据
     * @throws ModelNotFoundException
     * @throws DbException
     * @throws DataNotFoundException
     */
    public function get(): array
    {
        if ($this->entity->type == "dynamic") {
            $this->entity->options = $this->getDynamicOptions();
        }
        return $this->entity->options;
    }

    /**
     * 获取动态List<Option>数据
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    public function getDynamicOptions(): array
    {
        $result = FrappeSelect::load($this->request, $this->entity->config)->select();
        if ($result instanceof Collection) return $result->toArray();
        if (is_array($result)) return $result;
        return [];
    }
}