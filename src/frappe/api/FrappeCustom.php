<?php

namespace frappe\api;

use frappe\entity\CustomApiEntity;
use frappe\utils\ConfigUtil;
use InvalidArgumentException;
use think\Request;

class FrappeCustom
{
    /**
     * @var \app\Request|Request
     */
    protected $request;
    /**
     * @var CustomApiEntity
     */
    protected $entity;

    /**
     * @param Request $request
     * @param array $config
     */
    public function __construct(Request $request, array $config)
    {
        $this->request = $request;
        $this->entity = new CustomApiEntity($config);
    }

    /**
     * @param Request $request
     * @param array $config
     * @return FrappeCustom
     */
    public static function load(Request $request, array $config): FrappeCustom
    {
        return new FrappeCustom($request, $config);
    }

    /**
     * 执行
     * @return mixed
     */
    public function run()
    {
        $pathName = ConfigUtil::getFrappePathName();
        $className = "\\app\\$pathName\\{$this->entity->name}";
        if (!class_exists($className)) {
            throw new InvalidArgumentException("Miss Config {$this->entity->name}");
        }
        $class = new $className;
        if (!method_exists($class, 'handle')) {
            throw new InvalidArgumentException("Miss Config {$this->entity->name} invalid method");
        }
        return $class->handle($this->request);
    }
}