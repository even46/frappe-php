<?php

namespace frappe\api;

use frappe\entity\CreateApiEntity;
use frappe\utils\ConvertUtil;
use think\db\Query;
use think\facade\Db;
use think\Request;

class FrappeCreate
{
    /**
     * @var \app\Request|Request
     */
    public $request;
    /**
     * @var Query|Db
     */
    public $db;
    /**
     * @var CreateApiEntity
     */
    protected $entity;
    /**
     * @var array Post 数据
     */
    public $postData = [];
    /**
     * @var array Insert 数据
     */
    public $insertData = [];
    /**
     * @param Request $request
     * @param array $config
     */
    public function __construct(Request $request, array $config)
    {
        $this->request = $request;
        $this->entity = new CreateApiEntity($config);
        $this->db = Db::name($this->entity->tableName);
    }

    /**
     * 加载配置
     * @param Request $request
     * @param array $config
     * @return FrappeCreate
     */
    public static function load(Request $request, array $config): FrappeCreate
    {
        return new FrappeCreate($request, $config);
    }

    /**
     * 执行查询
     * @return int|string
     */
    public function insert()
    {
        $this->postData = $this->request->param() ?? [];
        # 1.解析参数 -> 固定参数、必须参数、转换参数
        # 2.
        $this->entity->fixedData = ConvertUtil::convertByGlobal($this->entity->fixedData);
        # 顺序：1-请求数据 2-合并默认数据-覆盖固定数据 4-字段数据验证 5-写入数据
        $this->mergeData()->rebuildData();
        $res = $this->db->insert($this->insertData);

        // 触发事件
        if ($res && $this->entity->afterEvents) {
            foreach ($this->entity->afterEvents as $afterEvent) {
                event($afterEvent);
            }
        }
        return $res;
    }

    /**
     * 合并数据
     * @return $this
     * @author yinxu
     * @date 2024/3/23 11:57:07
     */
    protected function mergeData(): FrappeCreate
    {
        # 合并默认参数值
        foreach ($this->entity->defaultData as $defKey => $defValue) {
            if (empty($defKey) || isset($this->postData[$defKey])) continue;
            $this->postData[$defKey] = $defValue;
        }
        # 合并固定参数值
        foreach ($this->entity->fixedData as $fixedKey => $fixedValue) {
            $this->postData[$fixedKey] = $fixedValue;
        }
        return $this;
    }

    /**
     * 重组&校验数据
     * @return $this
     * @author yinxu
     * @date 2024/3/23 11:56:38
     */
    protected function rebuildData(): FrappeCreate
    {
        $this->insertData = [];
        foreach ($this->entity->tableFields as $field) {
            $name = $field['name'] ?? "";
            $raw = $field['raw'] ?? "";
            $type = $field['type'] ?? "";
            $convert = $field['convert'] ?? "";
            $required = $field['required'] ?? false;
            $unique = $field['unique'] ?? false;
            if (empty($name) || empty($type)) continue;
            if ($required && !isset($this->postData[$name])) throw new \InvalidArgumentException("缺少[{$name}]参数");
            if (!isset($this->postData[$name])) continue;
            $value = $this->postData[$name] ?? null;
            # 转换数据格式
            $value = ConvertUtil::convert($convert, $value);
            # TODO：验证数据

            # 数据重新给到InsertData
            $this->insertData[$name] = $value;
        }
        return $this;
    }
}