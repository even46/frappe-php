<?php

namespace app\frappe\lib\constants;

class SelectMode
{
    const multiple = "multiple";
    const single = "single";
    const tags = "tags";
}