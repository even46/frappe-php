<?php

namespace app\frappe\lib\constants;

class FrappeOptionType
{
    const TYPE_STATIC = "static";
    const TYPE_DYNAMIC = "dynamic";
}