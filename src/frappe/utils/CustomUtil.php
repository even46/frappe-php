<?php

namespace frappe\utils;

use Exception;
use InvalidArgumentException;

class CustomUtil
{
    /**
     * 创建文件
     * @param string $name
     * @param string $phpCode
     * @return bool
     * @throws Exception
     * @author yinxu
     * @date 2024/3/23 17:55:35
     */
    public static function write(string $name, string $phpCode = ""): bool
    {
        $name = ucfirst($name);
        $path = app()->getAppPath() . ConfigUtil::getFrappePathName();
        $file = "$path/$name.php";

        if (!is_dir($path)) {
            @mkdir($path);
//        } else if (is_file($file)) {
//            throw new InvalidArgumentException("file: $name.php exist");
        }
//        $template = ConfigUtil::getTemplateContent();
//        $content = str_replace('{CLASS_NAME}', $name, $template);
//        $content = str_replace('{FUNCTION_CONTENT}', $phpCode ?: 'return "ok";', $content);
        $res = file_put_contents($file, $phpCode);
        if ($res == false) {
            throw new Exception("File Write Error: $file");
        }
        return true;
    }

    /**
     * 删除文件
     * @param string $name
     * @author yinxu
     * @date 2024/3/23 17:55:24
     */
    public static function delete(string $name)
    {
        $name = strtoupper($name);
        $path = app()->getAppPath() . ConfigUtil::getFrappePathName();
        $file = "$path/$name.php";
        if (is_file($file)) {
            @unlink($file);
        }
    }


}