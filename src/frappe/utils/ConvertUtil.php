<?php

namespace frappe\utils;

use think\facade\Log;

class ConvertUtil
{
    /**
     * 转换公共数据
     * @param array $data
     * @return array
     * @author yinxu
     * @date 2024/3/23 11:46:57
     */
    public static function convertByGlobal(array $data): array
    {
        foreach ($data as &$value) {
            if (is_array($value)) continue;
            //request.uid
            Log::debug("convertByGlobal: $value : " . (string)strpos($value, "request."));
            if (strpos($value, "request.") === 0) {
                $value = str_replace("request.", "", $value);
                $value = self::getRequestParam($value);
            }else if (strpos($value, 'range_before_year:') === 0) {
                $year = intval(str_replace("range_before_year:", "", $value));
                $value = [date('Y-m-d', strtotime("-$year years")), date('Y-m-d')];
            }else if (strpos($value, 'range_after_day:') === 0) {
                $day = intval(str_replace("range_after_day:", "", $value));
                $value = [date('Y-m-d'), date('Y-m-d', strtotime("+$day days"))];
            }else if (strpos($value, 'before_year:') === 0) {
                $year = intval(str_replace("before_year:", "", $value));
                $value = date('Y-m-d', strtotime('-3 years'));
            }else if (trim($value) == "date") {
                $value = date('Y-m-d');
            }else if (trim($value) == "datetime") {
                $value = date('Y-m-d H:i:s');
            }else if (function_exists($value)) {
                $value = $value();
            }
        }
        return $data;
    }

    public static function convertFixed(string $value)
    {
        if (strpos($value, "request.") === 0) {
            $value = str_replace("request.", "", $value);
            $value = self::getRequestParam($value);
        }else if (strpos($value, 'range_before_year:') === 0) {
            $year = intval(str_replace("range_before_year:", "", $value));
            $value = [date('Y-m-d', strtotime("-$year years")), date('Y-m-d')];
        }else if (strpos($value, 'range_after_day:') === 0) {
            $day = intval(str_replace("range_after_day:", "", $value));
            $value = [date('Y-m-d'), date('Y-m-d', strtotime("+$day days"))];
        }else if (strpos($value, 'before_year:') === 0) {
            $year = intval(str_replace("before_year:", "", $value));
            $value = date('Y-m-d', strtotime('-3 years'));
        }else if (trim($value) == "date") {
            $value = date('Y-m-d');
        }else if (trim($value) == "datetime") {
            $value = date('Y-m-d H:i:s');
        }else if (function_exists($value)) {
            $value = $value();
        }
        return $value;
    }

    public static function convertType(string $type, $value)
    {
        switch (strtolower($type)) {
            case 'number':
            case 'int':
                return (int) $value;
            case 'amount':
                return (float) $value;
            case 'string':
                return (string) $value;
            case 'array':
                return (array) $value;
        }
        return $value;
    }

    /**
     * 按数据转换成指定格式
     * @param $convert
     * @param $value
     * @return mixed
     * @author yinxu
     * @date 2024/3/23 11:47:28
     */
    public static function convert($convert, $value, $data = null)
    {
        if ($convert == 'json_decode') return json_decode($value, true);
        if ($convert == 'json_encode') return json_encode($value, 320);
        if ($convert == 'implode' && is_array($value)) return implode(',', $value);
        if ($convert == 'explode' && (is_string($value) || is_null($value))) return explode(',', $value ?: '');
        // if ($convert == 'region_to_lnglat') return region_to_lnglat($value);
        // TODO: 更多扩展
        if ($convert && function_exists($convert)) {
            return $convert($value);
        }
        return $value;
    }

    public static function getRequestParam(string $name, $default = null) {
        if (!str_contains($name, '.')) return request()->$name ?? $default;
        $name = explode('.', $name);
        $value = request()->{$name[0]};
        if (!is_array($value)) return $default;
        foreach ($name as $key => $val) {
            if ($key == 0) continue;
            if (is_array($value) && isset($value[$val])) {
                $value = $value[$val];
            } else {
                return $default;
            }
        }
        return $value;
    }
}